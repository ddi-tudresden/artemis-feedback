# ArTEMiS Feedback

This repository contains a hints module and examples to generate feedback suitable for novices in the ArTEMiS automatic assessment system (pyintro2022)

The folder progra2024 contains selected tasks, tests and feedback to highlight challenges when implementing given programming tasks for novices into the feedback system ArTEMiS.