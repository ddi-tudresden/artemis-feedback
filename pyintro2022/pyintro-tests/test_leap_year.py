import ast
from functools import partial
from pathlib import Path
from pytest import fail as pytest_fail
from hints import check_on_conditionals, check_on_constants, check_on_loops

fail = partial(pytest_fail, pytrace=False) # short version

try:
    from assignment.leap_year import is_leap_year
except ImportError as e:
    def is_leap_year(*args, **kwargs):
        fail("The function is_leap_year is missing in file leap_year.py")
except SyntaxError as e:
    fail("The function is_leap_year has syntax errors.")
    
srcFile = Path("assignment/leap_year.py")

def test_leap_year_exists():
    """File leap_year.py should exist."""
    assert srcFile.exists(), "File leap_year.py could not be found."
        
def test_leap_year_examples():
    for year in [1996, 2000, 2004, 2008]:
        if not is_leap_year(year):
            fail(f"Your function returns a wrong result for year {year} [Hint(s): {leap_year_hints()}]")

def test_no_leap_year_examples():
    for year in [1900, 2100, 2001, 2002, 2003]:
        if is_leap_year(year):
            fail(f"Your function returns a wrong result for year {year} [Hint(s): {leap_year_hints()}]")

def leap_year_hints():
    with open(srcFile, mode="rb") as stream:    # binary mode is more robust
        source = stream.read()
    try:
        root = ast.parse(source, filename=srcFile.name)
        hints_if = check_on_conditionals(root, "is_leap_year")
        hints_constants = check_on_constants(root, "is_leap_year")
        return ", ".join(hints_if + hints_constants)
    except SyntaxError as error:
        fail(f"The file {srcFile.name} has a syntax error: {error}")