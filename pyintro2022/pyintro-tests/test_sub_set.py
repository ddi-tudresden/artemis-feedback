import ast
import inspect
from functools import partial
from pathlib import Path
from pytest import mark
from pytest import fail as pytest_fail
from assignment import subset
from hints import check_on_loops

fileName = "subset.py"
function = "is_subset"
srcFile = Path(f"assignment/{fileName}")
fileNotFound = f"File {fileName} could not be found."

fail = partial(pytest_fail, pytrace=False) # damit man das nicht immer ausschreiben muss

def test_subset_exists():
    assert srcFile.exists(), fileNotFound
    
def test_subset():
    if (hasattr(subset, 'is_subset') and inspect.isfunction(subset.is_subset)):
        standard = "The algorithm is not yet correct."
        if subset.is_subset(["One","Two","Three"],["Two"]) != True:
            fail(f"{standard} Are you checking, if the second list is a subset of the first? [Hint(s): {subset_hints()}]")
        if subset.is_subset(["Four"],["One","Two","Three"]) != False:
            fail(f"{standard} Are you checking what happens, if the lists do not have a subset-relationship? [Hint(s): {subset_hints()}]")
        if subset.is_subset([],[]) != True:
            fail(f"{standard} Have you tried to pass an empty list? [Hint(s): {subset_hints()}]")
    else:
        fail(f"There is not yet a function {function} defined in file {fileName}")
        
def subset_hints():
    with open(srcFile, mode="rb") as stream:    # binary mode is more robust
        source = stream.read()
    try:
        root = ast.parse(source, filename=srcFile.name)
        hints_loops = check_on_loops(root, function, "Use loops to iterate over list elements.")
        return ", ".join(hints_loops)
    except SyntaxError as error:
        fail(f"The file {srcFile.name} has a syntax error: {error}")