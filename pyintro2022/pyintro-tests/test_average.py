import inspect
from functools import partial
from pathlib import Path
from pytest import mark
from pytest import fail as pytest_fail
from assignment import average
from hints import check_on_builtin_functions

fail = partial(pytest_fail, pytrace=False) # damit man das nicht immer ausschreiben muss
averageFile = Path("assignment/average.py")

def test_average_exists():
    """File average.py should exist."""
    assert averageFile.exists(), "File average.py could not be found"

def test_average():
    if (hasattr(average, "calculate_mean") and inspect.isfunction(average.calculate_mean)):
        failstring = f"If there are list elements of type string then None should be returned. Are you checking the type of the data input? [Hint(s): {hints_avgValue()}]"
        try:
            assert average.calculate_mean(["Test"]) == None
        except TypeError as error:
            fail(failstring)
        except AssertionError as error:
            fail(failstring)
    else:
        fail(f"There is no function calculate_mean defined in file average.py.")
        
def test_avgValues_correct():
    try:
        avgValues = [([], None), ([1,2,3], 2), ([3],3), ([3.5,7.5,9,2],5.5), ([2,4,6,8],5.0)]
        for list, entry in avgValues:
            if not average.calculate_mean(list) == entry:
                fail(f"For list {list} your function returns the wrong value. [Hint(s): {hints_avgValue()}]")
    except ZeroDivisionError as e:
        fail(f"For an empty list [] your source code raises an error. None should be returned. [Hint(s): {hints_avgValue()}]")

def hints_avgValue():
    try:
        hints = check_on_builtin_functions(averageFile, ["sum", "len"])
        return ", ".join(hints)
    except SyntaxError as error:
        fail(f"Your program has a syntax error: {error}")
        