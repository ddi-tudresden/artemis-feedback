import ast
from pprint import pprint
from unittest.mock import Mock

def check_on_conditionals(root, start, custon_msg = None):
    hints = []
    fun_node = search_function(start, root)
    if not any(isinstance(node, ast.If)
        for node in ast.walk(fun_node)):
            if not custon_msg:
                custon_msg = "use if/else for conditional statements"
            hints.append(custon_msg)
    return hints

def check_on_constants(root, start, custon_msg = None):
    hints = []
    fun_node = search_function(start, root)
    const_values = [node.value for node in ast.walk(fun_node)
        if isinstance(node, ast.Constant)]
    
    if not all((x in const_values) for x in [4, 100, 400]):
        if not custon_msg:
            custon_msg = "test divisibility by 4, 100 und 400"
        hints.append(custon_msg)
    return hints

def check_on_loops(root, start, custon_msg = None):
    hints = []
    fun_node = search_function(start, root)
    for_loops = True
    if not any(isinstance(node, ast.For) for node in ast.walk(fun_node)):
        for_loops = False
    if not any(isinstance(node, ast.While) for node in ast.walk(fun_node)):
        while_loops = False
    if not for_loops and not while_loops:
        if not custon_msg:
            custon_msg = "use while and for to implement loop statements"
        hints.append(custon_msg)
    return hints
    
def check_on_lists(root, start, custon_msg = None):
    hints = []
    fun_node = search_function(start, root)
    if not any(isinstance(node, ast.List) for node in ast.walk(fun_node)):
        if not custon_msg:
            custon_msg = "use lists to group elements"
        hints.append(custon_msg)
    return hints


"""
function_names is a dictionary with the name of the function, and custom messages
as corresponding value
"""
def check_on_builtin_functions(fileName, function_names):
    custom_messages = {
        "len": "with the function len you can return the number of elements of a list",
        "sum": "with the function sum the sum of the list elements can be returned",
        "print": "Please use the print function",
        "pow": "Please use the pow function"
    }
    
    with open(fileName, mode="rb") as stream: # binary mode is more robust
        source = stream.read()
        try:
            bytecode = compile(
                source,
                filename=fileName.name,
                mode="exec"
            )
            mock = Mock(wraps=len, name="len")
            eval(bytecode, {"len": mock})
            if mock.call_count == 0:
                return ["with the function len you can return the number of elements of a list"]
        except SyntaxError as error:
            fail(f"Your program has a syntax error: {error}")
    
#
# Helper functions for the ast module
#
def search_function(name, ast_node):
    for subnode in ast_node.body:
        if isinstance(subnode, ast.FunctionDef) and subnode.name == name:
            return subnode
    fail(f"Your source code does not contain a function {name}()")