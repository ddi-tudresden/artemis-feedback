# pyintro2022

This folder contains a hints module and examples to generate feedback suitable for novices in the ArTEMiS automatic assessment system.

The folder pyintro-exercise contain the template code to be compleded for three different programming tasks:

* averagy.py: Compute the average of a elements of a given list
* leap_year.py: Write a function which decides whether a given year is a leap year or not
* subset.py: Write a function which decides wether a given list is a subset of another given list

The folder pyintro-solution contains example solutions for those tasks. The folder pyintro-tests contain tests for automated feedback.