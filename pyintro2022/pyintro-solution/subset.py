def test(smaller, bigger):
    for k in smaller:
        if k in bigger:
            pass
        else:
            return False
    return True

def is_subset(list1: list, list2: list):
    result = None
    len1 = len(list1)
    len2 = len(list2)
    if len1 < len2:
        result = test(list1, list2)
    else:
        result = test(list2, list1)
    return result