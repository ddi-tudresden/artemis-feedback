def calculate_mean(number_list: list):
    length = len(number_list)
    if length == 0:
        return None
    for l in number_list:
        if type(l) != int:
            if type(l) != float:
                return None
    summe = sum(number_list)
    average = summe / length
    return average