import inspect
from functools import partial
from pathlib import Path
from pytest import mark
from pytest import fail as pytest_fail
from unittest.mock import Mock, MagicMock
from assignment import negativ_positiv

fail = partial(pytest_fail, pytrace=False) # damit man das nicht immer ausschreiben muss

file = Path("assignment/negativ_positiv.py")

name = "Negativ oder Positiv:"

def test_positiv_exists():
        """File summe.py should exist."""
        assert file.exists(), "Datei negativ_positiv.py wurde nicht gefunden"

def test_positiv():
    if (hasattr(negativ_positiv, "ist_positiv") and inspect.isfunction(negativ_positiv.ist_positiv)):
        tests = [(1,True), (0,True), (-6,False), (-3.2,False)]
        for elem in tests :
            if not negativ_positiv.ist_positiv(elem[0]) == elem[1]:
                fail(f"{name} Für das Argument {elem[0]} ergibt deine Funktion ein falsches Ergebnis.")
    else:
        fail(f"{name} In der Datei negativ_positiv.py ist noch keine Funktion ist_positiv definiert.")