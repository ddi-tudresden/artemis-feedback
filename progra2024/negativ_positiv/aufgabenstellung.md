# Funktionen

Erstelle eine Funktion, die eine Zahl als Parameter erhält. Die Funktion soll ```True``` liefern, wenn die Zahl größer oder gleich als 0 ist und ```False```, wenn die Zahl kleiner als 0 ist.

* Lösung bitte in Datei negativ_positiv.py einfügen
* Der Name der zu implementierenden Funktion lautet ```ist_positiv```. Sie erwartet genau einen Parameter.