def division(zahl = None):
    ergebnis = None
    fehlermeldung = "Deine Eingabe erzeugt einen Fehler"
    try:
        ergebnis = 5 / (zahl-3)
        return ergebnis
    except TypeError:
        print(fehlermeldung)
    except ZeroDivisionError:
        print(fehlermeldung)

if __name__ == '__main__':
    division("Python")
    division(5)
    division(3)
    division(3.0)