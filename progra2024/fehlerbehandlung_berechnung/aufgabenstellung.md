# Fehlerbehandlung

Schreibe eine Funktion ```division```, welche für das ihr übergebene Argument zahl die Rechnung ```5 / (zahl - 3)``` ausführt und ausgibt. Fange alle auftretenden Fehler ab und teste das Programm mit den Argumenten "Python", 5, 3 und 3.0.