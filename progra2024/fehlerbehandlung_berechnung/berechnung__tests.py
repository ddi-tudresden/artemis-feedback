import inspect
from functools import partial
from pathlib import Path
from pytest import mark
from pytest import fail as pytest_fail
from unittest.mock import Mock, MagicMock
from assignment.berechnung import division

fail = partial(pytest_fail, pytrace=False) # damit man das nicht immer ausschreiben muss

file = Path("assignment/berechnung.py")

name = "Berechnung:"

def test_berechnung_exists():
        """File berechnung.py should exist."""
        assert file.exists(), "Datei berechnung.py wurde nicht gefunden."

def test_berechnung():
    zeichenketten = ["Python", "", "20"]
    try:
        for z in zeichenketten:
            if division(z) is not None:
                fail(f"Für Zeichenketten wie \"{z}\" soll die Funktion None zurückgeben.")
    except TypeError:
        fail(f"\nDein Quellcode erzeugt einen Typfehler. Bitte überprüfe, ob deine Funktion den Fall betrachtet, wenn eine Zeichenkette als Argument übergeben wird.")

    zahlen = [(5, 2.5), (4,5)]
    for za in zahlen:
        if division(za[0]) != za[1]:
            fail(f"Die Berechnung ist für das Argument {za[0]} noch nicht korrekt.")
    try:
        zero = [3, 3.0]
        for o in zero:
            if division(o) is not None:
                fail(f"Die Funktion soll für das Argument {za[0]} None zurückgeben.")
    except ZeroDivisionError:
        fail(f"\nDein Quellcode erzeugt eine Division durch. Bitte überprüfe, ob du diesen Fall separat behandelst.")