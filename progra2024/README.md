## Aufgaben der LV Programmieren für das Lehramt SoSe 2024

Dieser Ordner enthält die Dateien, um zwei beispielhafte Aufgaben in das Feedback System ArTEMiS einzupflegen.

Die Ordner beinhalten die Aufgabenstellung, das Aufgabentemplate sowie eine Musterlösung. Außerdem die Testdatei zur Generierung automatischen Feedbacks.